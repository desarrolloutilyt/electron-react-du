import React from "react";
import "../styles/index.css";

const App = () => {
  return (
    <div>
      <h1>Electron React DU</h1>
      <p>Boilerplate starter Electron's project with React Hooks.</p>
    </div>
  );
};

export default App;
